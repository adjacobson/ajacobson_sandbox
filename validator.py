#!/usr/bin/python

# Importing the prerequisites
import dicom
import os
import numpy
from matplotlib import pyplot, cm


d1 = dicom.read_file("fwd_abcd/i00001.CFMRI.1")
d2 = dicom.read_file("rvs_abcd/i00001.CFMRI.1")
d3 = dicom.read_file("epi_abcd/i00001.CFMRI.1")


#dcmvals = "d1", "d2", "d3"
#for dcmval in dcmvals
#dcmval = d1
if "fwd" in d1.SeriesDescription:
	fwd_hd = d1

if "rvs" in d1.SeriesDescription:
	rvs_hd = d1

if "fMRI" in d1.SeriesDescription:
	epi_hd = d1

if "fwd" in d2.SeriesDescription:
	fwd_hd = d2

if "rvs" in d2.SeriesDescription:
	rvs_hd = d2

if "fMRI" in d2.SeriesDescription:
	epi_hd = d2

if "fwd" in d3.SeriesDescription:
	fwd_hd = d3

if "rvs" in d3.SeriesDescription:
	rvs_hd = d3

if "fMRI" in d3.SeriesDescription:
	epi_hd = d3
# Testing for differences between topup fwd and rvs

#parameters = "Rows", "Columns", "EchoNumbers", HighBit", "EchoTime", "FlipAngle", "PixelSpacing", "ImagedNucleus", "SliceLocation", "PixelBandwidth", "RepetitionTime"
parameters = "SAR",	"Rows", "Modality",	"Columns",	"HighBit", "InplanePhaseEncodingDirection",	"StudyID",	"EchoTime",	"ImageType",	"StudyDate",	"StudyTime",	"AngioFlag",	"FlipAngle",	"HeartRate",	"SeriesDate",	"SeriesTime",	"PatientID",	"PixelData",	"Laterality", "Manufacturer",	"StationName",	"BitsStored",	"PatientAge",	"PatientSex",	"ScanOptions",	"TriggerTime",	"SOPClassUID",	"WindowWidth",	"PatientName",	"ProtocolName",	"PixelSpacing",	"WindowCenter",	"SeriesNumber",	"ImagedNucleus", "TriggerWindow",	"BitsAllocated",	"PatientWeight", "SliceLocation",	"PixelBandwidth",	"RepetitionTime",	"SliceThickness",	"AccessionNumber",	"AcquisitionDate",	"AcquisitionTime",	"InstitutionName",	"SOPInstanceUID",	"InstanceNumber",	"PatientPosition",	"PercentSampling",	"SequenceVariant",	"EchoTrainLength",	"ImagingFrequency",	"ScanningSequence",	"SeriesDescription",	"SamplesPerPixel",	"AcquisitionMatrix",	"NumberofAverages",	"PerformedLocation",	"AcquisitionNumber",	"StudyInstanceUID",	"SeriesInstanceUID",	"DeviceSerialNumber",	"MRAcquisitionType",	"PixelRepresentation",	"SpecificCharacterSet",	"ImagesinAcquisition",	"SpacingBetweenSlices",	"ManufacturerModelName",	"PerformedStationName",	"FrameofReferenceUID",	"ImagePositionPatient",	"MagneticFieldStrength",	"ReconstructionDiameter", "ReferencedImageSequence",	"LargestImagePixelValue",	"PhotometricInterpretation",	"SmallestImagePixelValue",	"AdditionalPatientHistory",	"PercentPhaseFieldofView",	"NumberofTemporalPositions",	"PositionReferenceIndicator",	"TemporalPositionIdentifier",
for parameter in parameters:
	if (fwd_hd.data_element(parameter).value) != (rvs_hd.data_element(parameter).value):			
		print("%s: fwd and rvs topup PARAMETER MISMATCH!!!!!") % parameter 
	#else:
	#	print("forward and reverse topup variable %s match. Nice work!") % parameter 

# 0009 Group
paramvals = 0x1002,	0x1004,	0x1027,	0x1030,	0x1031,	0x10e9,
for paramval in paramvals:
	if fwd_hd[0x09,paramval].value != rvs_hd[0x09,paramval].value:			
		print(fwd_hd[0x09,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")
	#else:
	#	print(fwd_hd[0x09,paramval])
	#	print("forward and reverse topup variable match. Nice work!")

# 0019 Group
paramvals =	0x100f,	0x1011,	0x1012,	0x1017,	0x1018,	0x1019,	0x101a,	0x101b,	0x101e,	0x105a,	0x107d,	0x107e,	0x107f,	0x1081,	0x1084,	0x1087,	0x1088,	0x108a,	0x108b,	0x108d,	0x108f,	0x1090,	0x1091,	0x1092,	0x1093,	0x1094,	0x1095,	0x1096,	0x1097,	0x109b,	0x109c,	0x109d,	0x109e,	0x109f,	0x10a0,	0x10a1,	0x10a2,	0x10a3,	0x10a4,	0x10a7,	0x10a8,	0x10a9,	0x10aa,	0x10ab,	0x10ac,	0x10ad,	0x10ae,	0x10af,	0x10b0,	0x10b1,	0x10b2,	0x10b3,	0x10b4,	0x10b5,	0x10b6,	0x10b7,	0x10b8,	0x10b9,	0x10ba,	0x10bb,	0x10bc,	0x10bd,	0x10be,	0x10c0,	0x10c2,	0x10c3,	0x10c4,	0x10c5,	0x10c6,	0x10c7,	0x10c8,	0x10c9,	0x10ca,	0x10cb,	0x10cc,	0x10cd,	0x10ce,	0x10cf,	0x10d2,	0x10d3,	0x10d5,	0x10d7,	0x10d8,	0x10d9,	0x10df,	0x10e0,	0x10e2,	0x10f2,	0x10f9
for paramval in paramvals:
	if fwd_hd[0x19,paramval].value != rvs_hd[0x19,paramval].value:			
		print(fwd_hd[0x19,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")
	#else:
	#	print(fwd_hd[0x19,paramval])
	#	print("forward and reverse topup variable match. Nice work!")

# 0020 Group
#paramvals = 0x0956,	0x0957
#for paramval in paramvals:
#	if fwd_hd[0x20,paramval].value != rvs_hd[0x20,paramval].value:			
#		print(fwd_hd[0x20,paramval])
#		print("0020, %s: fwd and rvs topup PARAMETER MISMATCH!!!!!") % paramval

# 0021 Group
paramvals =	0x1035,	0x1036,	0x1037,	0x104f,	0x1050,	0x1051,	0x1052,	0x1053,	0x1056,	0x1057,	0x1058,	0x1059,	0x105a,	0x105b,	0x105c,	0x105d,	0x105e,	0x105f,	0x1081,	0x1082,	0x1083,	0x1084
for paramval in paramvals:
	if fwd_hd[0x21,paramval].value != rvs_hd[0x21,paramval].value:			
		print(fwd_hd[0x21,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")
	#else:
	#	print(fwd_hd[0x21,paramval])
	#	print("forward and reverse topup variable match. Nice work!")

# 0023 Group
paramvals =	0x1070,	0x1074,	0x107d,	0x1080
for paramval in paramvals:
	if fwd_hd[0x23,paramval].value != rvs_hd[0x23,paramval].value:			
		print(fwd_hd[0x23,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")
	#else:
	#	print(fwd_hd[0x23,paramval])
	#	print("forward and reverse topup variable match. Nice work!")

# 0025 Group
paramvals =	0x1006,	0x1007,	0x1010,	0x1011,	0x1014,	0x1017,	0x1018,	0x1019,	0x101a,	0x101b
for paramval in paramvals:
	if fwd_hd[0x25,paramval].value != rvs_hd[0x25,paramval].value:			
		print(fwd_hd[0x25,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")

# 0027 Group
paramvals =	0x1006,	0x1010,	0x1030,	0x1031,	0x1032,	0x1033,	0x1035,	0x1040,	0x1041,	0x1060,	0x1061,	0x1062
for paramval in paramvals:
	if fwd_hd[0x27,paramval].value != rvs_hd[0x27,paramval].value:			
		print(fwd_hd[0x27,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")

# 0029 Group
paramvals =	0x1015,	0x1016,	0x1017,	0x1018,	0x1026,	0x1034,	0x1035
for paramval in paramvals:
	if fwd_hd[0x29,paramval].value != rvs_hd[0x29,paramval].value:			
		print(fwd_hd[0x29,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")

# 0043 Group
paramvals =	0x1002,	0x1003,	0x1004,	0x1006,	0x1006,	0x1007,	0x1008,	0x1008,	0x1009,	0x1009,	0x100a,	0x100b,	0x100c,	0x100d,	0x100e,	0x1010,	0x101c,	0x101d,	0x1028,	0x1029,	0x102a,	0x102c,	0x102e,	0x102f,	0x1030,	0x1032,	0x1033,	0x1034,	0x1035,	0x1036,	0x1037,	0x1038,	0x1039,	0x1060,	0x1062,	0x106f,	0x107d,	0x1080,	0x1082,	0x1083,	0x1084,	0x1088,	0x1089,	0x108a,	0x1090,	0x1095,	0x1096,	0x1097,	0x1098,	0x109a,	0x10aa,	0x10b3
for paramval in paramvals:
	if fwd_hd[0x43,paramval].value != rvs_hd[0x43,paramval].value:			
		print(fwd_hd[0x43,paramval])
		print("fwd and rvs topup PARAMETER MISMATCH!!!!!")
	